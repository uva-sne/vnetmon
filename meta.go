package main

import (
	"log"

	"vnet.uvalight.net/utils/mqtt"
)

type M struct {
	Kind string            `json:"@"`
	KV   map[string]string `json:"m"`
}

type Pub struct {
	Key    string `json:"k"`
	Value  string `json:"v"`
	Retain bool   `json:"r"`
}

// Monitor MQTT-based metadata for a single connection attempt
func metadata(pub chan Pub, out chan Message, cancel chan struct{}) {
	wait := make(chan struct{}, 1)
	forwardMeta := func(k, v []byte) {
		// Channel can be closed before we get the opportunity to close the
		// MQTT client
		defer func() {
			if err := recover(); err != nil {
				log.Println("Metadata publish error:", err)
			}
		}()
		metas := MetaPair{string(k): string(v)}
		out <- M{"m", metas}
	}

	cli, err := mqtt.New("vnetmon")
	if err != nil {
		log.Fatalln("MQTT error:", err)
	}
	defer cli.Close()

	go func() {
		for {
			msg, ok := <-pub
			if !ok {
				break
			}
			if msg.Retain {
				cli.PubRetain(msg.Key, msg.Value)
			} else {
				cli.Pub(msg.Key, msg.Value)
			}
		}
	}()

	// TODO: limit what we forward
	err = cli.Sub("#", forwardMeta)
	if err != nil {
		log.Fatalln("MQTT sub error:", err)
	}

	select {
	case <-wait:
	case <-cancel:
	}

	cli.Disconnect()
}

/*
// Monitor Redis-based metadata for a single connection attempt
func metadata(out chan Message, cancel chan struct{}) {
	cli := redis.NewClient(&redis.Options{
		Network: "unix",
		Addr:    "/tmp/vnet/redis/redis.sock",
	})
	defer cli.Close()

	ps := cli.Subscribe("keys")
	defer ps.Close()

	// This isn't great, but works well enough
	ks, _ := cli.Keys("*").Result()
	metas := MetaPair{}
	for _, k := range ks {
		v, _ := cli.Get(k).Result()
		metas[k] = v
	}
	out <- Message{"metadata", metas}

	msgs := ps.Channel()
	ok := true
	for ok {
		select {
		case m := <-msgs:
			metas := MetaPair{}
			for _, k := range strings.Split(m.Payload, " ") {
				v, _ := cli.Get(k).Result()
				metas[k] = v
			}
			out <- Message{"metadata", metas}

		case <-cancel:
			ok = false
			break
		}
	}
}
*/
