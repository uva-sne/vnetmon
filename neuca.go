package main

import (
	"io/ioutil"
	"log"
	"os/exec"
	"strings"
)

type NeucaMeta struct {
	UnitURL      string
	PhysicalHost string
	SliceName    string
	ManagementIP string
}

// TODO:
//
// * Use slice_name or reservation_id to map to layer?
// * Report management_ip, *_id?
func NeucaData() (nm NeucaMeta) {
	var b []byte
	var err error

	if b, err = ioutil.ReadFile("/run/ec2-user-data"); err != nil {
		b, err = exec.Command("neuca-user-data").Output()
	}

	if err != nil {
		log.Printf("vnetmon :Failed to obtain user-data: %v", err)
		return
	}

	lines := strings.Split(string(b), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "unit_url=") {
			nm.UnitURL = line[9:]
		} else if strings.HasPrefix(line, "physical_host=") {
			nm.PhysicalHost = line[14:]
		} else if strings.HasPrefix(line, "slice_name=") {
			nm.SliceName = line[11:]
		} else if strings.HasPrefix(line, "management_ip=") {
			nm.ManagementIP = line[14:]
		}
	}

	return
}
