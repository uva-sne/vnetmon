package main

import (
	"errors"
	"io"
	"log"
	"sync"
	"time"

	"encoding/json"
	"github.com/gorilla/websocket"
)

var ErrClosed = errors.New("Connection closed")
var comTimeout = 30 * time.Second

type (
	Com struct {
		cancel chan struct{}
		ws     *websocket.Conn
		closed bool
		m      sync.RWMutex
		send   chan Message
	}
	Message interface{}
)

var dialer = websocket.Dialer{}

func NewCom(con string) (*Com, error) {
	c := &Com{}
	c.cancel = make(chan struct{}, 1)
	// TODO: deadline
	return c, c.Dial(con)
}

func (c *Com) Dial(host string) (err error) {
	c.ws, _, err = dialer.Dial(host, nil)
	if err != nil {
		return
	}
	c.ws.SetReadDeadline(time.Now().Add(comTimeout))
	c.ws.SetPingHandler(func(string) error {
		c.ws.SetReadDeadline(time.Now().Add(comTimeout))
		return nil
	})
	c.send = make(chan Message, 4)
	go c.sender()
	return
}

func (c *Com) Close() {
	c.m.Lock()
	defer c.m.Unlock()
	if !c.closed {
		c.closed = true
		close(c.send)
		close(c.cancel)
		c.ws.Close()
	}
}

func (c *Com) Read(v interface{}) error {
	for {
		// TODO: add keepalive
		mt, r, err := c.ws.NextReader()
		if _, ok := err.(*websocket.CloseError); ok {
			return io.EOF
		} else if err != nil {
			return err
		}
		if mt == websocket.TextMessage {
			return json.NewDecoder(r).Decode(v)
		} else {
			log.Println("vnetmon severity=error :Unknown message type:", mt)
		}
	}
}

func (c *Com) sender() {
	defer c.Close()
	for {
		pkt, ok := <-c.send
		if !ok {
			break
		}

		c.ws.SetWriteDeadline(time.Now().Add(comTimeout))
		err := c.ws.WriteJSON(pkt)
		if err != nil {
			log.Println("vnetmon severity=error :Send error:", err)
			break
		}
		//if n, ok := err.(net.Error); ok && n.Timeout() {
		//	break
		//}
	}
}

func (c *Com) Closed() bool {
	c.m.RLock()
	defer c.m.RUnlock()
	return c.closed
}

func (c *Com) Send(msg interface{}) {
	c.send <- msg
}
