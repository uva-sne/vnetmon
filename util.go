package main

import (
	"strconv"
	"time"
)

func timer(t *time.Ticker, f func()) {
	for {
		select {
		case _, ok := <-t.C:
			if !ok {
				return
			}
		}
		f()
	}
}

func split(s string) (ret []string) {
	start := 0
	f := false
	for i := 0; i < len(s); i++ {
		if s[i] == ' ' || s[i] == ':' {
			if f {
				ret = append(ret, s[start:i])
				f = false
			}
			start = i
		} else if !f {
			start = i
			f = true
		}
	}
	if f {
		ret = append(ret, s[start:len(s)])
	}
	return
}

func i64(s string) int64 {
	i, _ := strconv.ParseInt(s, 10, 64)
	return i
}

func hex2dec(b byte) int {
	if b >= '0' && b <= '9' {
		return int(b - '0')
	}
	if b >= 'a' && b <= 'f' {
		return int(b - 'a' + 10)
	}
	if b >= 'A' && b <= 'F' {
		return int(b - 'A' + 10)
	}
	return 0
}

func stringArrayEqual(left, right []string) bool {
	if len(left) != len(right) {
		return false
	}
	for i := 0; i < len(left); i++ {
		if left[i] != right[i] {
			return false
		}
	}
	return true
}

func stringArrayContains(a []string, k string) bool {
	for _, v := range a {
		if v == k {
			return true
		}
	}
	return false
}
