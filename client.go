// vnet controller client
package main

import (
	"fmt"
	"net"
	"os"
	"strings"

	"vnet.uvalight.net/utils/envconfig"
)

// Node-identifying information
type (
	Identity struct {
		FQDN      string `json:"fqdn"`
		Node      string `json:"node,omitempty"`
		NodeID    string `json:"node_id,omitempty"`
		SliceName string `json:"slice_name,omitempty"`
		IP        string `json:"management_ip,omitempty"`
		Physical  string `json:"parent,omitempty"`
	}
	I struct {
		Kind string   `json:"@"`
		Id   Identity `json:"i"`
	}
)

type MetaPair map[string]string

// Figure out FQDN, if available
func FQDN() string {
	addrs, err := net.LookupAddr("127.0.0.1")
	if err == nil {
		for _, v := range addrs {
			if v != "localhost" && !strings.HasPrefix(v, "localhost.") {
				return v
			}
		}
	}
	host, _ := os.Hostname()
	return host
}

func NewIdentity(config map[string]string) (Identity, error) {
	d := NeucaData()
	if d.UnitURL == "" {
		return Identity{}, fmt.Errorf("No unit URL defined (yet)")
	}

	hostname := FQDN()

	// TODO: we should probably just report all vnet_* properties
	node_id := config["vnet_node_id"]
	if node_id == "" {
		node_id = config["vnet_routing_asn"]
	}

	return Identity{hostname, d.UnitURL, node_id,
		d.SliceName, d.ManagementIP, d.PhysicalHost}, nil
}

func session(cancel chan struct{}) (string, error) {
	// Configuration may be updated externally
	cfg := envconfig.Load("/etc/vnet-config")
	server, _ := cfg["vnetmon_server"]
	if server == "" {
		return server, fmt.Errorf("Server not configured")
	}

	id, err := NewIdentity(cfg)
	if err != nil {
		return server, err
	}

	c, err := NewCom(server)
	if err != nil {
		return server, err
	}
	defer c.Close()

	c.Send(I{"i", id})

	pub := make(chan Pub)
	go rpc(c, pub)
	go metadata(pub, c.send, c.cancel)

	select {
	case <-cancel:
	case <-c.cancel:
	}

	close(pub)
	return server, nil
}
