package main

// TODO: maintain a mapping of interface <-> link for exogeni?

import (
	"log"
	"os"
	"os/signal"
	"time"

	"vnet.uvalight.net/utils"
)

var stopped = false

func interrupt(c chan os.Signal, cancel chan struct{}) {
	<-c
	if !stopped {
		close(cancel)
		stopped = true
		signal.Reset(os.Interrupt)
	}
}

func main() {
	utils.Syslog("vnetmon")

	cancel := make(chan struct{}, 1)
	sig := make(chan os.Signal, 1)
	go interrupt(sig, cancel)
	signal.Notify(sig, os.Interrupt)
	var last time.Time

	lastError := ""
	for !stopped {
		if time.Since(last) < 3*time.Second {
			time.Sleep(3 * time.Second)
		}

		last = time.Now()
		if server, err := session(cancel); err != nil {
			lastError = reportError(server, lastError, err.Error())
		}
	}
}

func reportError(server, last, cur string) string {
	if last != cur {
		log.Printf("vnetmon event=CONTROLLER_ERROR controller=%v severity=error :%v",
			server, cur)
	}

	return cur
}
