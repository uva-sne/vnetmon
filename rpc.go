package main

import (
	"encoding/json"
	"io"
	"log"
	"os/exec"
	"regexp"
	"strings"
	"sync"
)

var exclusiveLock = &sync.Mutex{}

// Controller messaging
type (
	ControllerMessage struct {
		Kind string          `json:"kind"`
		Data json.RawMessage `json:"data"`
	}
	Task struct {
		Task      string   `json:"task"`
		Args      []string `json:"args"`
		Input     string   `json:"input"`
		Exclusive bool     `json:"exclusive"`
	}
)

func rpc(c *Com, pub chan Pub) {
	var err error
	for {
		var m ControllerMessage
		if err = c.Read(&m); err != nil {
			break
		}

		if m.Kind == "task-request" || m.Kind == "itask-request" {
			var t Task
			if err = json.Unmarshal(m.Data, &t); err != nil {
				break
			}
			if m.Kind == "task-request" {
				go Task{Task: t.Task, Args: t.Args, Exclusive: t.Exclusive}.runSafe("vn-", nil)
			} else {
				go Task{Task: t.Task, Args: t.Args, Exclusive: t.Exclusive}.runSafe("vni-", &t.Input)
			}
		} else if m.Kind == "publish" {
			var p Pub
			if err = json.Unmarshal(m.Data, &p); err != nil {
				break
			}
			pub <- p
		} else {
			log.Printf("vnetmon severity=error :Not implemented: %v", m.Kind)
		}
	}
	if err != nil && err != io.EOF {
		log.Println("vnetmon severity=error :Processing error:", err)
	}
	c.Close()
}

// Run arbitrary commands from RPC
var rSafeCommand = regexp.MustCompile(`^[a-zA-Z0-9\-]+$`)

func (t Task) runSafe(prefix string, input *string) {
	if !rSafeCommand.MatchString(t.Task) {
		log.Println("vnetmon severity=error :Invalid command:", t.Task)
		return
	}
	if t.Exclusive {
		exclusiveLock.Lock()
		defer exclusiveLock.Unlock()
	}
	runLogged(prefix+t.Task, t.Args, input)
}

func runLogged(c string, args []string, input *string) (err error) {
	log.Printf("vnetmon event=COMMAND_START command=%q :%q", c, args)
	cmd := exec.Command(c, args...)
	if input != nil {
		var p io.WriteCloser
		p, err = cmd.StdinPipe()
		if err == nil {
			go func() {
				defer p.Close()
				io.Copy(p, strings.NewReader(*input))
			}()
		}
	}

	if err == nil {
		var out []byte
		out, err = cmd.CombinedOutput()
		if len(out) > 0 {
			lines := strings.Split(string(out), "\n")
			for _, line := range lines {
				log.Printf("vnetmon severity=error event=COMMAND_OUTPUT command=%q :%v", c, line)
			}
		}
	}
	if err != nil {
		log.Printf("vnetmon severity=error event=COMMAND_END command=%q :%v", c, err)
	} else {
		log.Printf("vnetmon event=COMMAND_END command=%q :Command succesful", c)
	}
	return err
}
